import {api} from "./api"

export const getTodoItems = () => {
    return api.get("/todo")
}

export const updateTodoTask = (id, todoTask) => {
    return api.put(`/todo/${id}`, todoTask)
}

export const deleteTodoTask = (id) => {
    return api.delete(`/todo/${id}`)
}

export const createTodoTask = (todoTask) => {
    return api.post(`/todo`, todoTask)
}

export const findTodoById=(id)=>{
    return api.get(`/todo/${id}`)
}

export const updateTodoState=(id,todoTask)=>{
    return api.put(`/todo/${id}`,todoTask)
}