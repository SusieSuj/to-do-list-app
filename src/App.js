import "./App.css"
import {NavLink, Outlet} from "react-router-dom"
import {BankOutlined, MenuOutlined, QuestionCircleOutlined} from "@ant-design/icons"

function App() {
    return (
        <div className="App">
            <div className="nav-bar">
                <nav>
                    <ul>
                        <li><NavLink to={"/"}><BankOutlined/></NavLink></li>
                        <li><NavLink to={"/done"}><MenuOutlined/></NavLink></li>
                        <li><NavLink to={"/help"}><QuestionCircleOutlined/></NavLink></li>
                    </ul>
                </nav>
            </div>
            <Outlet></Outlet>
        </div>
    )
}

export default App
