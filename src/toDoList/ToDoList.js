import AddToDoList from "./AddToDoList"
import ItemGroup from "./ItemGroup"
import {useEffect} from "react"
import {useTodo} from "../features/hooks/useTodo"

export default function ToDoList() {
    const {reloadTodoItem} = useTodo()
    useEffect(() => {
        async function fetchTodoData() {
            reloadTodoItem()
        }

        fetchTodoData()

    }, [])

    return (
        <div className="toDoListDiv">
            <h1>Todo List</h1>
            <ItemGroup/>
            <AddToDoList/>
        </div>
    )
}
