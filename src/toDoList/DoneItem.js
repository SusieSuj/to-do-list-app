import {useDispatch} from "react-redux"
import {deleteToDoList} from "../features/toDoListSlice"
import {useNavigate} from "react-router-dom"

export default function DoneItem({item}) {
    const dispatch = useDispatch()
    const navigation = useNavigate()

    const handleTaskNameClick = () => {
        navigation("/done/" + item.id)
    }

    const className = item.done ? "font-with-line" : "font-without-line"
    return (
        <div className="toDoItem" onClick={handleTaskNameClick}>
            <p className={className}> {item.text} </p>
        </div>
    )
}