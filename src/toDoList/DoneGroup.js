import {useSelector} from "react-redux"
import DoneItem from "./DoneItem"

export default function DoneGroup() {
    const toDoTasks = useSelector(state => state.toDoList.testList).filter(task => task.done)
    return (
        <div className="toDoListDiv">
            {toDoTasks.map((item) => <DoneItem key={item.id} item={item}></DoneItem>)}
        </div>

    )
}
