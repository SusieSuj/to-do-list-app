import Item from "./Item"
import {useSelector} from "react-redux"

export default function ItemGroup() {

    const list = useSelector(state => state.toDoList.testList)
    return (
        <div>
            {list.map((item) => <Item key={item.id} item={item}></Item>)}
        </div>

    )
}