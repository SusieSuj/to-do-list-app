import {useTodo} from "../features/hooks/useTodo"
import React, {useState} from "react"
import {Drawer, Modal, theme} from "antd"
import {DeleteOutlined, EditOutlined, EyeOutlined} from "@ant-design/icons"
import TextArea from "antd/es/input/TextArea"
import {findTodoById} from "../api/todo"


export default function Item({item}) {

    const {deleteTodo} = useTodo()
    const {updateTodo} = useTodo()
    const {switchTodoState} = useTodo()
    const className = item.done ? "font-with-line" : "font-without-line"
    const [isModalOpen, setIsModalOpen] = useState(false)
    const [editValue, setEditValue] = useState("")
    const [itemIdFound, setItemIdFound] = useState(-1)
    const [itemTextFound, setTextFound] = useState("")
    const [itemDoneFound, setItemDoneFound] = useState(false)


    const handleChange = async () => {
        await deleteTodo(item.id)
    }

    const clickTheItem = async () => {
        await switchTodoState(item.id, {
            id: item.id,
            text: item.text,
            done: !item.done
        })
    }

    const showModal = async () => {
        setEditValue(item.text)
        await setIsModalOpen(true)
    }
    const handleOk = async () => {
        setIsModalOpen(false)
        await switchTodoState(item.id, {
            id: item.id,
            text: editValue,
            done: item.done
        })
        updateTodo(item.id, {text: editValue, done: item.done})
    }
    const handleCancel = () => {
        setIsModalOpen(false)
        setEditValue("")
    }

    const showDetail = async () => {
        const itemFound = (await findTodoById(item.id))
        setItemIdFound(itemFound.data.id)
        setTextFound(itemFound.data.text)
        setItemDoneFound(itemFound.data.done)
        showDrawer()
    }
    const [open, setOpen] = useState(false)
    const showDrawer = () => {
        setOpen(true)
    }
    const onClose = () => {
        setOpen(false)
    }


    return (<div className="toDoItem">
        <p className={className} onClick={clickTheItem}>{item.text}</p>
        <div className="editAndDeleteButtonDiv">
            <EyeOutlined className="todoButton" type="dashed" size="large" onClick={showDetail}/>
            <EditOutlined className="todoButton" type="dashed" size="large" onClick={showModal}>Edit</EditOutlined>
            <DeleteOutlined className="todoButton" type="dashed" value="delete"
                            onClick={handleChange}>Delete</DeleteOutlined>
            <Drawer
                title="Todo Detail"
                placement="right"
                closable={false}
                onClose={onClose}
                open={open}
                getContainer={false}
            >
                <p>id: {itemIdFound}</p>
                <p>text: {itemTextFound}</p>
                <p>done: {itemDoneFound ? "finished" : "undo"}</p>
            </Drawer>

        </div>
        <Modal title="Edit todo" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
            <TextArea rows={4} placeholder={item.text} type="text" value={editValue}
                      onChange={event => setEditValue(event.target.value)}></TextArea>
        </Modal>
    </div>)
}