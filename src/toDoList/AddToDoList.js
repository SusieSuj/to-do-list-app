import {useState} from "react"
import {useTodo} from "../features/hooks/useTodo"
import {Button, Input} from "antd"

export default function AddToDoList() {
    const [inputValue, setValue] = useState("")
    const {createTodo} = useTodo()
    const handleChange = () => {
        createTodo({text: inputValue, done: false})
        setValue("")
    }

    return (
        <div className="addToDoItem">
            <Input size="large" type="text" value={inputValue} onChange={event => setValue(event.target.value)}/>
            <Button value="add" type="dashed" size="large" onClick={handleChange}>ADD</Button>
        </div>
    )
}