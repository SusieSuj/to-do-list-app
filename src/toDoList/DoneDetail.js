import {useParams} from "react-router-dom"
import {useSelector} from "react-redux"

export default function DoneDetail() {
    const {id} = useParams()

    const todoTask = useSelector((state) => state.toDoList.testList).find(item => item.id === id)

    return (
        <div className="toDoListDiv">
            <h1>Detail</h1>
            <div className="toDoItem">
                <div>{todoTask?.id}</div>
                <div>{todoTask?.text}</div>
            </div>
        </div>
    )
}