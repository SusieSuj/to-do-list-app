import DoneGroup from '../toDoList/DoneGroup'

export default function DoneList(){
    return (
        <div>
            <h1>Done list</h1>
            <DoneGroup/>
        </div>
    )
}