import {createSlice} from "@reduxjs/toolkit"

const toDoListSlice = createSlice({
    name: "toDoList",
    initialState: {
        testList: []
    },
    reducers: {
        initTodoList: (state, action) => {
            state.testList = action.payload
        }
    }
})

export default toDoListSlice
export const {deleteToDoList, initTodoList} = toDoListSlice.actions
