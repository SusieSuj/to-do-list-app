import * as todoService from "../../api/todo"
import {initTodoList} from "../toDoListSlice"
import {useDispatch} from "react-redux"

export const useTodo = () => {
    const dispatch = useDispatch()

    async function reloadTodoItem() {
        await todoService.getTodoItems().then(response => dispatch(initTodoList(response.data)))
    }

    const updateTodo = async (id, todoTask) => {
        await todoService.updateTodoTask(id, todoTask)
        await reloadTodoItem()
    }

    const deleteTodo = async (id) => {
        await todoService.deleteTodoTask(id)
        await reloadTodoItem()
    }

    const createTodo = async (todoTask) => {
        await todoService.createTodoTask(todoTask)
        await reloadTodoItem()
    }

    const switchTodoState=async (id,todoTask)=>{
        await todoService.updateTodoState(id,todoTask)
        await reloadTodoItem()
    }
    return {
        reloadTodoItem, updateTodo, deleteTodo, createTodo,switchTodoState,
    }
}