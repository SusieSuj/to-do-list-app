import React from "react"
import ReactDOM from "react-dom/client"
import "./index.css"
import App from "./App"
import reportWebVitals from "./reportWebVitals"
import store from "./app/store"
import {Provider} from "react-redux"
import {createBrowserRouter, RouterProvider} from "react-router-dom"
import ErrorPage from "./pages/ErrorPage"
import HelpPage from "./pages/HelpPage"
import DoneList from "./pages/DoneList"
import ToDoList from "./toDoList/ToDoList"
import DoneDetail from "./toDoList/DoneDetail"

const root = ReactDOM.createRoot(document.getElementById("root"))

const router = createBrowserRouter([
    {
        path: "/",
        element: <App/>,
        errorElement: <ErrorPage/>,
        children: [
            {
                index: true,
                element: <ToDoList/>
            },
            {
                path: "/done",
                element: <DoneList/>,
            },
            {
                path: "/help",
                element: <HelpPage/>,
            },
            {
                path: "/done/:id",
                element: <DoneDetail/>
            }
        ]
    },
])

root.render(
    <React.StrictMode>
        <Provider store={store}>
            {/*<App />*/}
            <RouterProvider router={router}/>
        </Provider>
    </React.StrictMode>
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
