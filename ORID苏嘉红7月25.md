# O

- The topic of today's study is: React, Redux
- In React, data can only be passed from parent components to child components. With Redux, data and method can be stored in a global repository, so we don't have to worry about parent-child relationships when data is transferred.
- In today's exercises and assignments, I learned some syntax and code specifications when using the React framework. For example, we can use the **export default function functionName** to replace the **const functionName = () =>{} ....export default functionName **. But I learned from the teacher that the arrow style at the back is more commonly used.

# R

- After keeping up with the pace of the teacher, I feel less confused, and I feel that the content of the class is basically understood

# I

- I feel that I mainly don't know enough about the writing and specification of grammar

# D

-  Go back and try to do a categorical summary, so that the search for knowledge is faster

