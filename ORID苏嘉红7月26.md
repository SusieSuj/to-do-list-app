# O

- The topic of today's study is: React Router, Frontend call API, ant design.
- React router keeps the UI in sync with the URL. We can set up that when we click on a connection, we jump to the corresponding component, just like the navigation bar of the website.
- The Frontend call API helps us test the functional interface of the frontend. We can use mockAPI to generate interfaces and data returned by the mock backend.

# R

- For me who can't design beautiful pages, ant design is really a very useful tool. It has a lot of different styles to choose from.

# I

- I don't think my aesthetic is very good, and I have to spend a lot of time trying and thinking about how to set the style.

# D

-  Go back tonight to continue modifying styles and learn how to adapt the page to resolution

