# O

- The topic of today's study is: HTML, CSS, React.

- Today's learning content about React is a little more complicated for me because I haven't learned the react framework before. I just got a brief look at React through the official documentation. 

- Something new I learned today:

  - We need to split the web page into components and analyze the relationships between the components.
  - How values are passed between components. In React, the passing of values is one-way and can only be passed to child components through parent components. If we want to pass a value to the parent in a child component, we need to call a method in the parent component.

# R

- I‘m not familiar with React. I felt lost in class and couldn't keep up.

# I

- I didn't know much about the front before, and I didn't touch the React framework. I read the official documentation to understand the react framework before, but in today's class, I feel that what I read before is useless.

# D

-  I need to learn the React framework with proper practice.

